// Key Notes:
//
// uANYTHING, and iANYTHING!
// + +% +|

// Key Remembers:
//
// To cast an integer to a different integer type, you use:
//     var other_num: u16 = @intCast(INTEGER);
// To cast a float to an integer type, you use:
//     var other_num: u16 = @intFromFloat(FLOAT);
// To cast an integer to a float type, you use:
//     var other_num: u16 = @floatFromInt(INTEGER);
// "_" can mean many things:
//     : Compiler figures out information. ( const my_array = [_]u8 { 1, 2, 3, 4, 5 }; )
//     : Ignore this value. ( _ = 5; )
// You can use !void in the main function to return nothing, or you can use !u8 to return an exit code!

const std = @import("std");

pub fn main() !u8 {
    //-- Hello, world! --//
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    try stdout.print("Hello, world!\n", .{});
    try bw.flush();

    //-- Printing Channels --//
    try stdout.print("This prints to STDOUT!\n", .{});
    try bw.flush();

    std.debug.print("This prints to STDERR!\n", .{});

    //-- Numeric Operations --//
    const num_1: u8 = 250;
    const num_2: u8 = 8;

    //-- _ = num_1 + num_2; // Does not compile! (Overflow error.) --//
    const thing_1 = num_1 +% num_2; // Out: 2
    const thing_2 = num_1 +| num_2; // Out: 255

    std.debug.print("250 +% 8 = {}\n", .{ thing_1 });
    std.debug.print("250 +| 8 = {}\n", .{ thing_2 });

    //-- Arrays --//
    const my_array = [5]u8 { 1, 2, 3, 4, 5 };
    const my_array_compiler_figures_out_size = [_]u8 { 1, 2 };
    const my_repeated_array = my_array ** 2;
    const my_concat_array = my_array ++ .{ 9, 8, 7 };

    var my_undefined_array: [2]u8 = undefined;

    my_undefined_array[0] = 2;
    my_undefined_array[1] = 5; // Commenting one of these, the code will still compile and run!

    const my_multi_array: [5][2]u8 = [_][2]u8 {
        .{ 1, 2 },
        .{ 3, 4 },
        .{ 5, 6 },
        .{ 7, 8 },
        .{ 9, 10 },
    };

    // This array would technically be: { 1, 2, 8 }
    // But it identifies as an array with len 2.
    const my_sentinal_array: [2:8]u8 = .{ 1, 2 };

    std.debug.print("Array: {any} | Length: {}\n", .{ my_array, my_array.len });
    std.debug.print("Array (Compiler Figured Out Size): {any} | Length: {}\n", .{ my_array_compiler_figures_out_size, my_array_compiler_figures_out_size.len });
    std.debug.print("Array (Repeated): {any} | Length: {}\n", .{ my_repeated_array, my_repeated_array.len });
    std.debug.print("Array (Concatenated): {any} | Length: {}\n", .{ my_concat_array, my_concat_array.len });
    std.debug.print("Array (Slowly Defined): {any} | Length: {}\n", .{ my_undefined_array, my_undefined_array.len });
    std.debug.print("Array (Multi-Dimensional): {any} | Length: {}\n", .{ my_multi_array, my_multi_array.len });
    std.debug.print("Array (Sentinal Terminated): {any} | Length: {}\n", .{ my_sentinal_array, my_sentinal_array.len });

    //-- Bools --//
    const t: bool = true;
    const f: bool = false;

    const tInt: u8 = @intFromBool(t);
    const fInt: u8 = @intFromBool(f);

    std.debug.print("True (Bool): {} | True (Int): {}\n", .{t, tInt});
    std.debug.print("False (Bool): {} | False (Int): {}\n", .{f, fInt});

    //-- Optionals (NULL) --//
    var maybe_byte: ?u8 = null;

    std.debug.print("Maybe Byte (NULL): {?}\n", .{ maybe_byte });

    maybe_byte = 42;

    std.debug.print("Maybe Byte (FILLED): {?}\n", .{ maybe_byte });

    // Assert 'maybe_byte' is not NULL!
    var the_byte = maybe_byte.?;

    // If 'maybe_byte' is NULL: 15. Else: VALUE
    the_byte = maybe_byte orelse 15;
    the_byte = if (maybe_byte) |v| v else 15; // The same thing essentially.

    std.debug.print("The Byte: {}\n", .{ the_byte });

    //-- If Statements --//
    // Ops: == != > >= < <= etc...
    if (1 == 1) {
        std.debug.print("Good, the computer can do basic math!\n", .{});
    }

    else {
        std.debug.print("No! Bad computer!\n", .{});
    }

    if (5 >= 4 and 3 <= 4) {
        std.debug.print("Indeed!\n", .{});
    }

    if (5 > 2 and 2 < 3) {
        std.debug.print("So true....\n", .{});
    }

    if (7 == 7 or 2 == 9) {
        std.debug.print("7 == 7\n", .{});
    }

    // One line if statement. :-)
    if ( 5 == 5 ) std.debug.print("5 == 5\n", .{});

    //-- If Statements On Optionals (NULL) --//
    var maybe_a_byte: ?u8 = null;

    if (maybe_a_byte) |_| {
        std.debug.print("This showing up is a bug!\n", .{});
    }

    else {
        std.debug.print("As expected, NULL!\n", .{});
    }

    maybe_a_byte = 38;

    if (maybe_a_byte) |v| {
        std.debug.print("Byte: {}\n", .{ v });
    }

    else {
        std.debug.print("This showing up is a bug!\n", .{});
    }

    //-- Blocks --//
    {
        // This variable only exists in this block.
        const xb: u8 = 1;

        std.debug.print("XB: {}\n", .{ xb });
    }

    // Getting a value from a block and storing it in this variable.
    const from_expr_x: u8 = blk: {
        const x: u8 = 5;
        const y: u8 = 7;
        const z: u8 = 26;

        // from_expr = x + y + z
        break :blk x + y + z;
    };

    std.debug.print("From EXPR X: {}\n", .{ from_expr_x });

    //-- Switch Statement --//
    const sx: u21 = 67;

    switch (sx) {
        0...5 => std.debug.print("Could be a 0, could be a 5, or something in the middle. (Range inclusive!)\n", .{}),

        31, 32, 35 => std.debug.print("Either 31, 32, or 35.\n", .{}),

        7...10 => |n| std.debug.print("It's a {}!\n", .{ n }),

        77 => {
            std.debug.print("Calculating in a block! :)\n", .{});
        },

        // As long as it is comptime known, you can have a block expression as the match value!
        blk: {
            const xt: u21 = 100;
            const yt: u21 = 2;

            break :blk xt + yt;
        } => std.debug.print("It's 102!\n", .{}),

        else => std.debug.print("Well, you have hit the else! Congrats! :)\n", .{}),
    }

    // Switch as an expression.
    const thing_x: u8 = 105;
    const from_switch_expr: u8 = switch (thing_x) {
        105 => 4,
        42 => 8,
        else => 69,
    };

    std.debug.print("From Switch EXPR: {}\n", .{ from_switch_expr });

    // Ignore this, this is just the main() return.
    return 0;
}
